'use strict'

let num1 = 87

if (typeof num1 === 'number') {
	console.log('Ця змінна має тип "number"')
} else {
	console.log('Ця змінна не має тип "number"')
}

const name = 'Arseniy'
const lastName = 'Shestopalov'

console.log(`Мене звати ${name} ${lastName}.`)

const numericVariable = 90

console.log(`The value of the numeric variable is: ${numericVariable}.`)
