let strArray = ["travel", "hello", "eat", "ski", "lift"]

let meter = 0

strArray.forEach(element => {
    if (element.length > 3) {
        meter++
    }
})

console.log(meter)

let users = [
    {
        name: "Арсенiй",
        age: 20,
        sex: "чоловiча",
    },
    {
        name: "Аркадiй",
        age: 23,
        sex: "чоловiча",
    },

    {
        name: "Олена",
        age: 36,
        sex: "жiноча",
    },
    {
        name: "Марiна",
        age: 29,
        sex: "жiноча",
    },
]

let filterArray = users.filter(element => element.sex === "чоловiча")

console.log(filterArray)

let bigArray = [
    "Test", "World", "Code", null,
    { "b": 2 }, { "c": 3 }, true, { "key": "value" },
    42, 24, 7, false, null,
    [3, 4], [7, 8], [5, 6], 13, null,
    { "a": 1 }, null, "Hello", true, false, [1, 2]
]

function filterByType(bigArray, type) {
    return bigArray.filter(element => typeof element === type);
}

let booleanValues = filterByType(bigArray, "boolean");

console.log(booleanValues);