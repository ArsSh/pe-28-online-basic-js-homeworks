"use strict";

let entFirstNum;
let entSecNum;

do {
    entFirstNum = prompt("Enter the first number");
    entFirstNum = Number(entFirstNum);
} while (isNaN(entFirstNum) || !Number.isInteger(entFirstNum));

do {
    entSecNum = prompt("Enter the second number");
    entSecNum = Number(entSecNum);
} while (isNaN(entSecNum) || !Number.isInteger(entSecNum));

if (entFirstNum > entSecNum) {
    for (let a = entSecNum; a <= entFirstNum; a++) {
        console.log(a);
    }
} else {
    for (let b = entFirstNum; b <= entSecNum; b++) {
        console.log(b);
    }
}

let entNum;

do {
    entNum = prompt("Enter a paired number");
    entNum = Number(entNum);
} while (isNaN(entNum) || !Number.isInteger(entNum) || entNum % 2 !== 0);
//изначально я написал тут Number.isInteger(entNum % 2 === 0), но потом понял что там проверяеться число, а мое выражение дает булевое значение, короче пока что задачи решать сложно((

console.log(entNum);
