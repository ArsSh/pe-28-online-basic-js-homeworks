/* 
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?

event.key возвращает нажатую клавищу в виде стоки


2. Яка різниця між event.code() та event.key()?

event.key() возвращает "F", название клавиши в виде строки 
event.code() возвращает "keyF", саму клавишу на клавиатуре 


3. Які три події клавіатури існує та яка між ними відмінність?

keydown когда пользователь нажимает клавишу на клавиатуре
keyup когда пользователь отпускает клавишу 
keypress когда нажатие клавиши возвращает символ 


Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
Але якщо при натискані на кнопку її  не існує в розмітці, то попередня активна кнопка повина стати неактивною.
*/

document.addEventListener('keydown', function (event) {
	const buttons = document.querySelectorAll('.btn')
	let checkKey = false

	buttons.forEach(button => {
		if (
			button.textContent.toLowerCase() === event.key.toLowerCase() ||
			(event.key === 'Enter' && button.textContent === 'Enter') ||
			(event.key === 'Tab' && button.textContent === 'Tab')
		) {
			button.style.backgroundColor = 'blue'
			checkKey = true
		} else {
			button.style.backgroundColor = ''
		}
	})

	if (!checkKey) {
		buttons.forEach(button => {
			button.style.backgroundColor = 'black'
		})
	}
})
