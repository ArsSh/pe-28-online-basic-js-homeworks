/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?

setTimeout задает таймер на одиарное выполнение куска кода 
setInterval задает интервал в который будет выполняться кусок кода многократно


2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

setTimeout => clearTimeout 
setInterval => clearInterval


Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати,
 що операція виконана успішно.

Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/

document.querySelector('.btn').addEventListener('click', function () {
	setTimeout(function () {
		const textChange = document.querySelector('.text-container')
		textChange.textContent = 'Text has changed'
	}, 3000)
})

let startCount = 10
const timer = document.querySelector('.timer')

const timerInterval = setInterval(() => {
	startCount--
	timer.textContent = startCount

	if (startCount <= 0) {
		clearInterval(timerInterval)
		timer.textContent = 'countdown complete'
	}
}, 1000)
