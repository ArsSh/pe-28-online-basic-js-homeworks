"use strict";

let age = prompt("enter your age: ");

let whoYouAre =
    age >= 18
        ? "you are an adult."
        : age >= 12
        ? "you are a teenager."
        : "you are a child.";

alert(whoYouAre);

let month = prompt("enter the month of the year in Ukrainian");

switch (month) {
    case "лютий":
        console.log("this month has 28 or 29 days");
        break;
    case "січень":
    case "березень":
    case "травень":
    case "липень":
    case "серпень":
    case "жовтень":
    case "грудень":
        console.log("this month has 31 days");
        break;
    case "квітень":
    case "червень":
    case "вересень":
    case "листопад":
        console.log("this month has 30 days");
        break;

    default:
        console.log("enter the name of the month correctly");
}
