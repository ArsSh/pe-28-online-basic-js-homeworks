/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?

События это куски кода которые начинают свое выполнение когда пользователь делает что то запланированное ранее: кликает по курсору, наводит курсор куда-то или убирает и тп.


2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.

click при клике 
dblclick при двойном клике
mousemove при перемещении над элементом
mouseover при перемещении на элемент или на дочерний элемент 
mouseout при уходе с элемента
mousedown при нажатии на элемент 
mouseup при отпускании курсора с элемента


3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

contextmenu срабатывает когда пользователь нажимает правую кнопку мыши на странице и вызывает контекстное меню
мы можем так же добавлять в контекстное меню свои опции 


Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

const btn = document.getElementById('btn-click')

btn.addEventListener('click', function () {
	const newParagraph = document.createElement('p')
	newParagraph.style.marginTop = '20px'
	newParagraph.textContent = 'New Paragraph'
	const parentContent = document.getElementById('content')
	parentContent.appendChild(newParagraph)
	parentContent.appendChild(newButton)
})

const newButton = document.createElement('button')
newButton.setAttribute('id', 'btn-input-create')
newButton.textContent = 'And, click me'
Object.assign(newButton.style, {
	display: 'block',
	backgroundColor: '#007bff',
	color: '#fff',
	padding: '10px 20px',
	border: 'none',
	cursor: 'pointer',
})

const parentContent = document.getElementById('content')
Object.assign(parentContent.style, {
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
})

newButton.addEventListener('click', function () {
	const addEntryField = document.createElement('input')
	addEntryField.setAttribute('type', 'text')
	addEntryField.setAttribute('placeholder', 'write here')
	addEntryField.setAttribute('name', 'entryField')
	Object.assign(addEntryField.style, {
		display: 'block',
		marginTop: '15px',
		padding: '10px 15px',
		border: '1px solid black',
		transition: 'border-color 0.3s, box-shadow 0,3s',
	})

	parentContent.insertBefore(addEntryField, newButton.nextSibling)
})
