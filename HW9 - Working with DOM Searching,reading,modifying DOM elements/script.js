/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)

dom это дерево или описание структуры html документа.
dom состоит из узлов(node) в которых хранитсья вся информация про страницу. 


2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML позволяет получать или менять структуру Html-элемента 
innerText позволяет получать или менять только тектовое содержимое элемента 


3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

getElementsByClassName('имя_класса') возвращает все элементы которые имеют указанный класс
getElementById('уникальный_идентификатор') если нужен быстрый доступ и прямой доступ к одному элементу по его уникальному идентификатору
getElementsByTagName('div') возвращает все элементы с которые соответствуют указанному тегу
querySelector('#уникальный_идентификатор') возвращает первый подходящий результат 
querySelectorAll('.имя_класса') возвращает все элементы которые соответствуют указанному селектору


4. Яка різниця між nodeList та HTMLCollection?

оба являються колекциями для хранения списков узлов или html-элементов в DOM-дереве
но:
nodeList может хранить все типы узлов
HTMLCollection только объекты типа Element




Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */



let styleSwapTextClass = document.getElementsByClassName('feature')

for (let i = 0; i < styleSwapTextClass.length; i++) {
    styleSwapTextClass[i].style.textAlign = 'center'
}

console.log(styleSwapTextClass)


let styleSwapTextCelector = document.querySelectorAll('.feature')

styleSwapTextCelector.forEach(function (styleSwapTextCelector) {
    styleSwapTextCelector.style.textAlign = 'center'
})

console.log(styleSwapTextCelector)

let swapTextCont = document.getElementsByTagName('h2')

for (let i = 0; i < swapTextCont.length; i++) {
    swapTextCont[i].innerText = 'Awesome feature'
}

let addSignInTheEnd = document.getElementsByClassName('feature-title')

for (let i = 0; i < addSignInTheEnd.length; i++) {
    addSignInTheEnd[i].textContent += '!'
}