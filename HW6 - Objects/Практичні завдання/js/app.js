let product = {
    name: "название товара",
    price: 1234,
    discount: 13,

    finalPrice: function () {
        return this.price - (this.price * this.discount / 100)
    },
}

console.log(product.finalPrice())

let user = {
}

let userName = prompt("Enter your name:")
user.name = userName

let userAge = prompt("Enter your age:")
userAge = Number(userAge)
user.age = userAge

function seyHello() {
    alert(`Hello my name is ${user.name}, i'm ${user.age} years old`)
}

seyHello()