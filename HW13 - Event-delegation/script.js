/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?

event. preventDefault() дает возможность открывать ссылки не меняя страницу 


2. В чому сенс прийому делегування подій?

Делегирование событий позволяет нам присоединить оброботчик событий прямо к род-элементу а не к для каждого доч-элемента отдельно 


3. Які ви знаєте основні події документу та вікна браузера? 

resize при изменении размера окна
scroll при скролинги окна 
load при полной загрузки страницы 


Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

document
	.querySelector('.centered-content')
	.addEventListener('click', function (event) {
		if (event.target.classList.contains('tabs-title')) {
			const tabsTitle = document.querySelectorAll('.tabs-title')
			const contentBlocks = document.querySelectorAll('.tabs-content li')

			contentBlocks.forEach(function (content) {
				content.style.display = 'none'
			})

			tabsTitle.forEach(function (tab) {
				tab.classList.remove('active')
			})

			event.target.classList.add('active')

			const tabName = event.target.getAttribute('data-tab')
			const activeContent = document.querySelector(
				'.tabs-content li[data-tab="' + tabName + '"]'
			)

			if (activeContent) {
				activeContent.style.display = 'block'
			}
		}
	})
