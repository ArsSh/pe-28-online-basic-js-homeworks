let enterStr = prompt("Enter the string")

function isPalindrome(enterStr) {
    let proStingArr = enterStr.split('')
    let reversArr = proStingArr.reverse()
    let reversStr = reversArr.join('')
    if (reversStr.toLowerCase() === enterStr.toLowerCase()) {
        console.log("The entered string is a palindrome")
    } else {
        console.log("The entered string is not a palindrome")
    }
}

console.log(isPalindrome(enterStr))

let enterString = prompt("Enter the string")

function stringLength(enterString, maxLength) {
    let strLength = enterString.length
    if (strLength <= maxLength) {
        return true
    } else {
        return false
    }
}

console.log(stringLength(enterString, 20))

let userBirthdate = prompt("Enter your date of birth in DD.MM.YYYY format");
let parts = userBirthdate.split(".");
let formatDate = `${parts[2]}-${parts[1]}-${parts[0]}`;
let birthDate = new Date(formatDate);

let date = {
    userDate: birthDate,
    nowDate: new Date(),

    calcFullAge: function () {

        let age = this.nowDate.getFullYear() - this.userDate.getFullYear();
        let monthDifference = this.nowDate.getMonth() - this.userDate.getMonth();
        if (monthDifference < 0 || (monthDifference === 0 && this.nowDate.getDate() < this.userDate.getDate())) {
            age--;
        }
        return age;
    }
};

console.log(date.calcFullAge());
