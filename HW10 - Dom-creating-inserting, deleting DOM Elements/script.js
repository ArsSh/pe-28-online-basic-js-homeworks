/* 

Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

createElement создает новый елемент, но не добавляет его в дерево DOM,
добавить его в дерево нужно будет самому, методами insertBefore и appendChild.
предварительно получив ссылку на род-элемент и его первый доч-узел

const p1 = document.createElement('p')
const p2 = document.createElement('p')

p1.textContent
p2.textContent

const род_элемент = document.getElementById('род_элемент')
const первый_доч_узел = род_элемент.childNodes[0]

род_элемент.insertBefore(p1, первый_доч_узел)
род_элемент.appendChild(p2)

insertBefore передаеться сам вставляемые элемент и ссылка на узел
appendChild добавляет елемент в конец списка узлов данного элемента


2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

const deletedElement = document.querySelector('.navigation')
if (deletedElement) {
    deletedElement.remove()
}


3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

insertBefore
appendChild

insertAdjacentHTML()
insertAdjacentElement()
insertAdjacentText()
с свойством position:
beforebegin перед элементом
afterbegin внутри элемента, перед первым доч-узлом
beforeend внутри элемента, перед последним доч-узлом
afterend после элемента


Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

 */

const learnMoreLink = document.createElement('a')
learnMoreLink.style.textDecoration = 'none'
learnMoreLink.style.color = '#fff'

learnMoreLink.textContent = 'Learn More'
learnMoreLink.setAttribute('href', '#')

const footer = document.querySelector('footer')
const paragraph = footer.querySelector('p')

paragraph.after(learnMoreLink)

const newElemSelect = document.createElement('select');
newElemSelect.setAttribute('id', 'rating');

const main = document.querySelector('main');
const features = main.getElementsByClassName('features')[0];

main.insertBefore(newElemSelect, features);

function addOption(value, text) {
    const option = document.createElement('option');

    option.value = value;
    option.textContent = text;

    document.getElementById('rating').appendChild(option);
}

addOption("5", "5 Stars");
addOption("4", "4 Stars");
addOption("3", "3 Stars");
addOption("2", "2 Stars");
addOption("1", "1 Star");


